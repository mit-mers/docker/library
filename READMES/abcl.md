- [Supported Tags](#org28ab289)
- [Quick Reference](#org6db5d8f)
- [What is ABCL?](#org047ca80)
- [What's in the image?](#orgcf3ce2a)
- [License](#org54405c7)



<a id="org28ab289"></a>

# Supported Tags

-   `1.7.1-debian-buster`, `1.7.1-debian`, `1.7.1`, `latest`


<a id="org6db5d8f"></a>

# Quick Reference

-   **ABCL Home Page:** <https://abcl.org/>
-   **Where to file Docker image related issues:** <https://gitlab.com/mit-mers/docker/abcl/>
-   **Where to file issues for build infrastructure:** <https://gitlab.com/mit-mers/docker/library>
-   **Where to file issues for ABCL itself:** <https://github.com/armedbear/abcl/issues>
-   **Maintained by:** [Eric Timmons](https://github.com/daewok) and the [MIT MERS Group](https://mers.csail.mit.edu/) (i.e., this is not an official ABCL image)
-   **Supported platforms:** `linux/amd64`
-   **Previously known as:** `daewok/abcl`


<a id="org047ca80"></a>

# What is ABCL?

From [ABCL's Home Page](https://abcl.org)

> Armed Bear Common Lisp (ABCL) is a full implementation of the Common Lisp language featuring both an interpreter and a compiler, running in the JVM. Originally started to be a scripting language for the J editor, it now supports JSR-223 (Java scripting API): it can be a scripting engine in any Java application. Additionally, it can be used to implement (parts of) the application using Java to Lisp integration APIs.


<a id="orgcf3ce2a"></a>

# What's in the image?

This image contains ABCL binaries released by the upstream devs.


<a id="org54405c7"></a>

# License

ABCL is licensed under the [GNU GPL](https://www.gnu.org/copyleft/gpl.html) with [Classpath exception](https://www.gnu.org/software/classpath/license.html).

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.

